#!/usr/bin/make -f

#export DH_VERBOSE=1
export PYBUILD_NAME=ffcx
export PYBUILD_DISABLE=test
#export PYBUILD_TEST_ARGS={dir}/test
#export PYBUILD_TEST_PYTEST=1

DEB_SOURCE_PACKAGE := $(strip $(shell egrep '^Source: ' debian/control | cut -f 2 -d ':'))
DEB_VERSION := $(shell dpkg-parsechangelog | egrep '^Version:' | cut -f 2 -d ' ')
DEB_NOEPOCH_VERSION := $(shell echo $(DEB_VERSION) | cut -d: -f2-)
DEB_UPSTREAM_VERSION := $(shell echo $(DEB_NOEPOCH_VERSION) | sed 's/-[^-]*$$//')
DEB_STRIPPED_UPSTREAM_VERSION = $(shell echo $(DEB_UPSTREAM_VERSION) | sed -e 's/\+dfsg.*$$//p')

FENICS_RELEASE_VERSION=$(DEB_STRIPPED_UPSTREAM_VERSION)
FENICS_MAJOR_VERSION=$(shell echo $(FENICS_RELEASE_VERSION) | sed "s/^\([^.]*\)\..*$$/\1/")
FENICS_MINOR_VERSION=$(shell echo $(FENICS_RELEASE_VERSION) | sed "s/^\([^.]*\)\.\([^.]*\)\..*$$/\2/")
FENICS_VERSION=$(FENICS_MAJOR_VERSION).$(FENICS_MINOR_VERSION)
FENICS_NEXT_VERSION=$(FENICS_MAJOR_VERSION).$(shell echo $$(( $(FENICS_MINOR_VERSION) + 1 )) )

USCAN_DESTDIR := $(CURDIR)

PYTHON_BASE_SCRIPT="import sys; base=list(filter( lambda p: p.startswith('/usr/lib/python') and p.endswith('/dist-packages'), sys.path ))[0]; sys.stdout.write(base)"
PYTHON3_MODULE_BASE := $(shell python3 -c $(PYTHON_BASE_SCRIPT))

PY3VER_DEFAULT := $(shell py3versions -dv)

%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild

override_dh_compress:
	dh_compress -X.py -X.ufl

override_dh_clean:
	dh_clean ffcx/git_commit_hash.py test/*.pdf test/*.h test/*.c test/compile-cache/
	rm -rf *.egg-info

override_dh_auto_clean:
	dh_auto_clean
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	$(MAKE) -C doc clean
endif

override_dh_auto_install:
	dh_auto_install
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
	PYTHONPATH=$(CURDIR)/debian/python3-ffcx/usr/lib/python$(PY3VER_DEFAULT)/dist-packages:${PYTHONPATH} $(MAKE) -C doc html man
endif

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
override_dh_python3:
	cd test; \
	PYTHONPATH=$(CURDIR)/debian/python3-ffcx/usr/lib/python$(PY3VER_DEFAULT)/dist-packages:${PYTHONPATH} \
	  PATH=$(CURDIR)/debian/python3-ffcx/usr/bin:$(PATH) \
	  pytest-3
	dh_python3
endif

override_dh_gencontrol:
	dh_gencontrol -- -Vfenics:Upstream-Version=$(FENICS_VERSION) -Vfenics:Next-Upstream-Version=$(FENICS_NEXT_VERSION)~

.PHONY: get-orig-source
get-orig-source:
	uscan --force-download --verbose --destdir $(USCAN_DESTDIR)
